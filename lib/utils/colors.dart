import 'package:flutter/material.dart';

const primaryColor = Color(0xff008208);

const grayColor = {
  '1000': Color(0xff1f2933),
  '900': Color(0xff323F4B),
  '800': Color(0xff3E4C59),
  '700': Color(0xff52606d),
  '600': Color(0xff616E7C),
  '500': Color(0xff7B8794),
  '400': Color(0xff9AA5B1),
  '300': Color(0xffCBD2D9),
  '200': Color(0xffE4E7EB),
  '100': Color(0xffF5F7FA),
};
