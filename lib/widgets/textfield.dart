import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:museum_rfid/utils/colors.dart';

class CustomTextField extends StatelessWidget {
  final String label;
  final TextInputType inputType;
  final bool isPassword;
  final Function(String) onChangedFn;
  String? Function(String?)? validatorFn;
  final String? errorTxt;
  int? maxLen;
  Iterable<String>? autofillType;
  void Function()? onEditingComplete;
  TextInputAction? textInputAction;

  CustomTextField({
    super.key,
    required this.label,
    required this.inputType,
    this.isPassword = false,
    required this.onChangedFn,
    this.errorTxt =
        'no error', // don't display red border around text input if there is no error
    this.validatorFn,
    this.maxLen,
    this.autofillType,
    this.onEditingComplete,
    this.textInputAction,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLength: maxLen,
      validator: validatorFn,
      onChanged: onChangedFn,
      keyboardType: inputType,
      obscureText: isPassword,
      style: GoogleFonts.inter(
        color: grayColor['800'],
        fontSize: 16,
        fontWeight: FontWeight.w600,
      ),
      cursorColor: grayColor['800'],
      autofillHints: autofillType,
      onEditingComplete: onEditingComplete,
      textInputAction: textInputAction,
      decoration: InputDecoration(
        contentPadding:
            const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        labelText: label,
        errorText: errorTxt == 'no error' ? null : errorTxt,
        labelStyle: GoogleFonts.inter(
          color: grayColor['500'],
          fontWeight: FontWeight.w500,
          fontSize: 14,
        ),
        filled: true,
        fillColor: Colors.white,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(
            width: 1.5,
            color: (grayColor['200'])!,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(
            width: 1.5,
            color: (grayColor['400'])!,
          ),
        ),
      ),
    );
  }
}
