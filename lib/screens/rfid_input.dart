import 'package:flutter/material.dart';
import 'package:museum_rfid/utils/colors.dart';
import 'package:museum_rfid/widgets/buttons.dart';
import 'package:museum_rfid/widgets/textfield.dart';
import 'package:flutter/services.dart';

class RFIDInputScreen extends StatelessWidget {
  const RFIDInputScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.light,
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(
          color: grayColor['800'],
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 20,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 10,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Complete setup",
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                        color: grayColor['1000'],
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Text(
                      "Enter your RFID Tag # below",
                      style: TextStyle(
                        fontSize: 16,
                        height: 1.5,
                        fontWeight: FontWeight.w500,
                        color: grayColor['800'],
                      ),
                    ),
                    const SizedBox(
                      height: 48,
                    ),
                    CustomTextField(
                      label: "RFID Tag #",
                      inputType: TextInputType.number,
                      onChangedFn: (String val) {},
                    )
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    PrimaryButton(
                      text: "Explore the Museum",
                      onPressedFn: () {
                        Navigator.of(context).pushNamed('/setup-success');
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
