import 'package:flutter/material.dart';
import 'package:museum_rfid/utils/colors.dart';
import 'package:museum_rfid/widgets/buttons.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:museum_rfid/providers/settings_provider.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({super.key});

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool isClosedCaption = false;
  bool isAudioDescription = false;
  bool isAutoplay = false;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => SettingsProvider(),
      child: Scaffold(
        appBar: AppBar(
          systemOverlayStyle: const SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarBrightness: Brightness.light,
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          iconTheme: IconThemeData(
            color: grayColor['800'],
          ),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 10,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Customize your experience",
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w700,
                          color: grayColor['1000'],
                        ),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Text(
                        "Select from the options below to improve your Museum experience",
                        style: TextStyle(
                          fontSize: 16,
                          height: 1.5,
                          fontWeight: FontWeight.w500,
                          color: grayColor['800'],
                        ),
                      ),
                      const SizedBox(
                        height: 48,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          const SettingsOption(
                            icon: Icons.closed_caption_outlined,
                            text: "Closed Caption",
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 16),
                            child: Switch(
                              // This bool value toggles the switch.
                              value: isClosedCaption,
                              activeColor: primaryColor,
                              onChanged: (bool value) {
                                // This is called when the user toggles the switch.
                                setState(() {
                                  isClosedCaption = value;
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          const SettingsOption(
                            icon: Icons.queue_music,
                            text: "Audio Description",
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 16),
                            child: Switch(
                              // This bool value toggles the switch.
                              value: isAudioDescription,
                              activeColor: primaryColor,
                              onChanged: (bool value) {
                                // This is called when the user toggles the switch.
                                setState(() {
                                  isAudioDescription = value;
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          const SettingsOption(
                            icon: Icons.play_circle_outline,
                            text: "Autoplay",
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 16),
                            child: Switch(
                              // This bool value toggles the switch.
                              value: isAutoplay,
                              activeColor: primaryColor,
                              onChanged: (bool value) {
                                // This is called when the user toggles the switch.
                                setState(() {
                                  isAutoplay = value;
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                      !isAutoplay
                          ? const SizedBox(
                              height: 0,
                            )
                          : Padding(
                              padding: const EdgeInsets.only(left: 24.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Select Track",
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: grayColor['500'],
                                        fontWeight: FontWeight.w500),
                                  ),
                                  const SizedBox(
                                    height: 16,
                                  ),
                                  SizedBox(
                                    height: 150,
                                    child: ListView(
                                      scrollDirection: Axis.horizontal,
                                      children: const <Widget>[
                                        AutoplayTrackCard(
                                          imageURL: "oceans8.png",
                                          name: "#Oceans8",
                                          index: 1,
                                        ),
                                        AutoplayTrackCard(
                                          name: "Tour of Precipitation",
                                          imageURL: "tourofppt.png",
                                          index: 2,
                                        ),
                                        AutoplayTrackCard(
                                          name: "Resilient Community",
                                          imageURL: "resilientcommunity.png",
                                          index: 3,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      PrimaryButton(
                        text: "Continue",
                        onPressedFn: () {
                          Navigator.of(context).pushNamed('/rfid-input');
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class AutoplayTrackCard extends StatelessWidget {
  final String imageURL;
  final String name;
  final int index;
  const AutoplayTrackCard({
    super.key,
    required this.name,
    required this.imageURL,
    required this.index,
  });

  @override
  Widget build(BuildContext context) {
    return Consumer<SettingsProvider>(builder: (context, model, child) {
      return Padding(
        padding: const EdgeInsets.only(right: 20, bottom: 10),
        child: GestureDetector(
          onTap: () {
            model.selectedIndex = index;
          },
          child: Container(
            height: 150,
            width: 134,
            decoration: BoxDecoration(
              color: grayColor['100'],
              borderRadius: BorderRadius.circular(15),
              border: Border.all(
                color: model.selectedIndex == index
                    ? primaryColor.withOpacity(0.5)
                    : grayColor['100']!,
                width: 3,
              ),
              boxShadow: const [
                BoxShadow(
                  blurRadius: 5,
                  offset: Offset(0, 4),
                  spreadRadius: 0,
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                ),
              ],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  "assets/$imageURL",
                  // height: 50,
                ),
                const SizedBox(
                  height: 16,
                ),
                Text(
                  name,
                  style: TextStyle(
                    fontSize: 12,
                    color: grayColor['800'],
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}

class SettingsOption extends StatelessWidget {
  final String text;
  final dynamic icon;
  const SettingsOption({super.key, required this.text, required this.icon});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Icon(
          icon,
          size: 28,
          color: grayColor['500'],
        ),
        const SizedBox(
          width: 16,
        ),
        Text(
          text,
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w600,
            color: grayColor['900'],
          ),
        ),
      ],
    );
  }
}
